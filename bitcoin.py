#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2017-18 Richard Hull and contributors
# See LICENSE.rst for details.

import cbpro, time
from datetime import datetime, timedelta
from itertools import islice

key="xxxx"
secret="xxxx"
passwd="xxxx"
auth_client = cbpro.AuthenticatedClient(key, secret, passwd)

import re
import time
import argparse

from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, SINCLAIR_FONT, LCD_FONT


lasteth = 0.01
lastbtc = 0.01
def device_init(n, block_orientation, rotate, inreverse):
    # create matrix device
    global device
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=n or 1, block_orientation=block_orientation,
                     rotate=rotate or 0, blocks_arranged_in_reverse_order=inreverse)
    print("Created device")

def demo(n, block_orientation, rotate, inreverse):
    # create matrix device
    global device,lasteth,lastbtc

    data = auth_client.get_product_ticker(product_id='ETH-DAI')
#    print data['time'], data['price']
    eth = data['price']
    data = auth_client.get_product_ticker(product_id='BTC-USD')
#    print data['time'], data['price']
    btc = data['price']

    words = [
            "E:"+str(float("{0:.2f}".format(float(lasteth))))+" B:"+str(float("{0:.2f}".format(float(lastbtc)))),
            "E:"+str(float("{0:.2f}".format(float(eth))))+" B:"+str(float("{0:.2f}".format(float(btc))))

    ]

    if ((eth != lasteth) or (btc != lastbtc)):
        device.contrast(16)
        print "ETH", data['time'], eth, " BTC ", btc
        virtual = viewport(device, width=device.width, height=len(words) * 8)
        with canvas(virtual) as draw:
            for i, word in enumerate(words):
#                text(draw, (0, i * 8), word, fill="white", font=proportional(CP437_FONT))
                text(draw, (0, i * 8), word, fill="white", font=proportional(LCD_FONT))

        for i in range(virtual.height - device.height +1):
            virtual.set_position((0, i))
            time.sleep(0.05)

    lasteth = eth
    lastbtc = btc

if __name__ == "__main__":
    device_init(12, -90, 0, False)
    while (1):
        try:
            demo(12, -90, 0, False)
        except:
            pass
        time.sleep(30)